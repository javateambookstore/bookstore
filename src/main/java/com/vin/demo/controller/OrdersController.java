package com.vin.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.vin.demo.service.OrdersService;
import com.vin.demo.entity.OrderItem;
import com.vin.demo.entity.OrderItemBootstrapTableJson;
import com.vin.demo.entity.Orders;
import com.vin.demo.service.OrderItemService;

@Controller
public class OrdersController {
	@Autowired
    private OrdersService OrdersService;
	@Autowired
    private OrderItemService OrderItemService;
	
	 @RequestMapping(value = {"Orders" })
     public ModelAndView getAllOrders() {		 
	 	List<Orders> ordersList = OrdersService.getAllOrders();
        return new ModelAndView("OrderManagement", "ordersList", ordersList);
     }
	 
	 @RequestMapping(value = { "Orders/Item" })
     public @ResponseBody OrderItemBootstrapTableJson getOrderItemByOrderId(@RequestParam int orderId) {	
		 OrderItemBootstrapTableJson json = new OrderItemBootstrapTableJson();
		 List<OrderItem> rows = OrderItemService.getOrderItemsByOrderId(orderId);
		 json.setRows(rows);
		 json.setTotal(rows.size());
        return json;
     }
}
