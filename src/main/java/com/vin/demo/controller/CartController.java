package com.vin.demo.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.vin.demo.entity.Book;
import com.vin.demo.entity.CommonConstants;
import com.vin.demo.entity.OrderItem;
import com.vin.demo.entity.Orders;
import com.vin.demo.service.BookService;
import com.vin.demo.service.OrdersService;

@Controller
public class CartController {
	private static final Logger logger = Logger.getLogger(CategoryController.class);
	@Autowired
	private BookService BookService;
	@Autowired
	private OrdersService orderService;
	@RequestMapping(value = "addToCart")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void addItem(@RequestParam int bookId, HttpSession session) {
		// Set order
		Orders order = (Orders) session.getAttribute(CommonConstants.CART);
		if (order == null) {
			order = new Orders();
			// session.setAttribute(CommonConstants.CART, newOrder);
		}
		Book book = BookService.getBook(bookId);
		List<OrderItem> orderItems = order.getOrderItems();
		if (orderItems == null)
			orderItems = new ArrayList<OrderItem>();

		for (int i = 0; i < orderItems.size(); i++) {
			if (book.getId() == orderItems.get(i).getBook().getId()) {
				OrderItem orderItem = orderItems.get(i);
				orderItem.setQuantity(orderItem.getQuantity() + 1);
				orderItem.setTotalPrice(book.getPrice().multiply(new BigDecimal(orderItem.getQuantity())));
				session.setAttribute(CommonConstants.CART, order);
				return;
			}
		}

		OrderItem orderItem = new OrderItem();
		orderItem.setBook(book);
		orderItem.setQuantity(1);
		orderItem.setPrice(book.getPrice());
		orderItem.setTotalPrice(book.getPrice().multiply(new BigDecimal(1)));
		orderItem.setOrder(order);
		orderItems.add(orderItem);
		order.setOrderItems(orderItems);

		// Caculate grand total
		BigDecimal grandTotal = new BigDecimal(0);
		for (OrderItem item : orderItems) {
			grandTotal = grandTotal.add(item.getTotalPrice());
		}
		order.setGrandTotal(grandTotal);
		// end Caculate grand total

		session.setAttribute(CommonConstants.CART, order);

	}

	@RequestMapping(value = { "deleteToCart" })
	public ModelAndView deleteCart(@RequestParam int bookId, HttpSession session) {
		// Set order
		Orders order = (Orders) session.getAttribute(CommonConstants.CART);
		if (order == null) {
			order = new Orders();
			// session.setAttribute(CommonConstants.CART, newOrder);
		}
		Book book = BookService.getBook(bookId);
		List<OrderItem> orderItems = order.getOrderItems();
		if (orderItems == null)
			orderItems = new ArrayList<OrderItem>();

		for (int i = 0; i < orderItems.size(); i++) {
			if (book.getId() == orderItems.get(i).getBook().getId()) {
				orderItems.remove(i);
			}
		}
		order.setOrderItems(orderItems);

		// Caculate grand total
		BigDecimal grandTotal = new BigDecimal(0);
		for (OrderItem item : orderItems) {
			grandTotal = grandTotal.add(item.getTotalPrice());
		}
		order.setGrandTotal(grandTotal);
		// end Caculate grand total

		session.setAttribute(CommonConstants.CART, order);
		return new ModelAndView("cart", "order", order);
	}

	@RequestMapping(value = { "editToCart" })
	public ModelAndView deleteCart(@RequestParam int bookId, @RequestParam int value, HttpSession session) {
		int addition = value;
		// Set order
		Orders order = (Orders) session.getAttribute(CommonConstants.CART);
		if (order == null) {
			order = new Orders();
			// session.setAttribute(CommonConstants.CART, newOrder);
		}
		Book book = BookService.getBook(bookId);
		List<OrderItem> orderItems = order.getOrderItems();
		if (orderItems == null)
			orderItems = new ArrayList<OrderItem>();

		for (int i = 0; i < orderItems.size(); i++) {
			if (book.getId() == orderItems.get(i).getBook().getId()) {

				OrderItem orderItem = orderItems.get(i);
				if (addition == -1 && orderItem.getQuantity() == 1) {
					// remove to list
					orderItems.remove(i);
				} else {
					orderItem.setQuantity(orderItem.getQuantity() + addition);
					orderItem.setTotalPrice(book.getPrice().multiply(new BigDecimal(orderItem.getQuantity())));
				}
				break;
			}
		}

		// Caculate grand total
		BigDecimal grandTotal = new BigDecimal(0);
		for (OrderItem item : orderItems) {
			grandTotal = grandTotal.add(item.getTotalPrice());
		}
		order.setGrandTotal(grandTotal);
		// end Caculate grand total
		session.setAttribute(CommonConstants.CART, order);
		return new ModelAndView("cart", "order", order);
	}

	@RequestMapping(value = { "cart" })
	public ModelAndView getCart(HttpSession session) {
		Orders order = (Orders) session.getAttribute(CommonConstants.CART);
		if (order == null)
			order = new Orders();
		return new ModelAndView("cart", "order", order);
	}
	
	@RequestMapping(value = {"checkOut" })
	public ModelAndView getCheckOut(HttpSession session) {
		Orders order = (Orders) session.getAttribute(CommonConstants.CART);
		if (order == null)
			order = new Orders();
		return new ModelAndView("checkOut", "order", order);
	}
	
	
	 @RequestMapping("saveCheckOut")
	    public ModelAndView saveCheckOut(@ModelAttribute Orders order,HttpSession session) {
	        logger.info("Saving the Order. Data : "+order);
	    	Orders orderSesion = (Orders) session.getAttribute(CommonConstants.CART);
	    	orderSesion.setCustomerAddress(order.getCustomerAddress());
	    	orderSesion.setCustomerName(order.getCustomerName());
	    	orderSesion.setEmail(order.getEmail());
	    	orderSesion.setPhoneNumber(order.getPhoneNumber());
	    	orderSesion.setShippingAddress(order.getShippingAddress());
	    	long id = orderService.insertOrder(orderSesion);
	    	if(id>0)
	    		session.setAttribute(CommonConstants.CART, new Orders());
	    	
	        return new ModelAndView("thanks");
	    }
}
