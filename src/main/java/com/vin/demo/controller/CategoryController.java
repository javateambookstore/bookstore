package com.vin.demo.controller;

import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.vin.demo.entity.Category;
import com.vin.demo.service.CategoryService;

@Controller
public class CategoryController {
	private static final Logger logger = Logger.getLogger(CategoryController.class);

	public CategoryController() {
		System.out.println("CategoryController()");
	}

	@Autowired
	private CategoryService categoryService;

	@RequestMapping("createCategory")
	public ModelAndView createProduct(@ModelAttribute Category category) {
		logger.info("Creating Category. Data: " + category);
		return new ModelAndView("categoryForm");
	}
	
	
	 @RequestMapping("deleteCategory")
	    public ModelAndView deleteProduct(@RequestParam int categoryId) {
	        logger.info("Deleting the Category. Id : "+categoryId);
	        categoryService.deleteCategory(categoryId);
	        return new ModelAndView("redirect:getAllCategories");
	    }
    
    @RequestMapping("editCategory")
    public ModelAndView editProduct(@RequestParam int categoryId, @ModelAttribute Category category) {
        logger.info("Updating the Category for the Id "+categoryId);
        category = categoryService.getCategory(categoryId);
        return new ModelAndView("categoryForm", "categoryObject", category);
    }
    
    @RequestMapping("saveCategory")
    public ModelAndView saveCategory(@ModelAttribute Category category) {
        logger.info("Saving the category. Data : "+category);
        if(category.getCategoryId() == 0){ // if Product id is 0 then creating the Product other updating the Product
        	categoryService.createCategory(category);
        } else {
        	categoryService.updateCategory(category);
        }
        return new ModelAndView("redirect:getAllCategories");
    }

	@RequestMapping(value = { "getAllCategories"})
	public ModelAndView getAllCategories() {
		logger.info("Getting the all Categories.");
		List<Category> categoryList = categoryService.getAllCategorys();
		return new ModelAndView("categoryList", "categoryList", categoryList);
	}

}
