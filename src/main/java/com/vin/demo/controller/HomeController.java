package com.vin.demo.controller;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.vin.demo.entity.Book;
import com.vin.demo.entity.CommonConstants;
import com.vin.demo.entity.Orders;
import com.vin.demo.service.BookService;
@Controller
public class HomeController {
	@Autowired
    private BookService BookService;
	 @RequestMapping(value = { "Home","/" })
	    public ModelAndView getHome() {
		 
		 	List<Book> BookList = BookService.getAllBooks();
//		 	Orders namnn = new Orders(); 
//		 	namnn.setCustomerName("NGUYEN NGOC NAM");
//		 	session.setAttribute(CommonConstants.CART, namnn);
//		 	
//		 	
//		 	Orders cart = (Orders) session.getAttribute(CommonConstants.CART);
//		 	
	        return new ModelAndView("home", "bookList", BookList);
	    }
	 @RequestMapping(value = { "Admin" })
	    public ModelAndView getAdmin() {
	        return new ModelAndView("admin");
	    }
	 
	 @RequestMapping(value="getString")
	    public @ResponseBody List<Book> getString()
	    {
		 	List<Book> BookList = BookService.getAllBooks();
	        return BookList;
	    }
	    
}
