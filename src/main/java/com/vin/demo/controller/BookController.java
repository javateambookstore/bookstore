package com.vin.demo.controller;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.vin.demo.entity.Category;
import com.vin.demo.entity.OrderItem;
import com.vin.demo.entity.Book;
import com.vin.demo.entity.BookModel;
import com.vin.demo.service.CategoryService;
import com.vin.demo.service.BookService;

@Controller
public class BookController {
	private static final Logger logger = Logger.getLogger(BookController.class);
    
    public BookController() {
        System.out.println("BookController()");
    }

    @Autowired
    private BookService BookService;
	@Autowired
	private CategoryService categoryService;

    @RequestMapping("createBook")
    public ModelAndView createBook(@ModelAttribute Book book) {
        logger.info("Creating Book. Data: "+book);
        Category category =categoryService.getCategory(book.getCategory_id());
        book.setBookCategory(category);
        
        BookModel BookModel = new BookModel();
        BookModel.setBook(book);
        List<Category> categoryList = categoryService.getAllCategorys();
        BookModel.setCategories(categoryList);
        
        return new ModelAndView("bookForm","bookObject",BookModel);
    }
    
    @RequestMapping("editBook")
    public ModelAndView editBook(@RequestParam int BookId, @ModelAttribute Book book) {
        logger.info("Updating the Book for the Id "+BookId);
        book = BookService.getBook(BookId);
        Category category =categoryService.getCategory(book.getCategory_id());
        book.setBookCategory(category);
        
        //build BookModel 
        BookModel BookModel = new BookModel();
        BookModel.setBook(book);
        List<Category> categoryList = categoryService.getAllCategorys();
        BookModel.setCategories(categoryList);
        
        return new ModelAndView("bookForm", "bookObject", BookModel);
    }
    
    @RequestMapping("saveBook")
    public ModelAndView saveBook(@ModelAttribute Book Book) {
        logger.info("Saving the Book. Data : "+Book);
         SecureRandom random = new SecureRandom();
         String randomCode = new BigInteger(130, random).toString(32);
        //Category category =categoryService.getCategory(Book.getCategory_id());
        //Book.setBookCategory(category);
        if(Book.getId() == 0){ // if Book id is 0 then creating the Book other updating the Book
        	//genareate
        	Book.setCode(randomCode);
            BookService.createBook(Book);
        } else {
            BookService.updateBook(Book);
        }
        return new ModelAndView("redirect:getAllBooks");
    }
    
    @RequestMapping("deleteBook")
    public ModelAndView deleteBook(@RequestParam int BookId) {
        logger.info("Deleting the Book. Id : "+BookId);
        BookService.deleteBook(BookId);
        return new ModelAndView("redirect:getAllBooks");
    }
    
    @RequestMapping(value = {"getAllBooks"})
    public ModelAndView getAllBooks() {
        logger.info("Getting the all Books.");
        List<Book> BookList = BookService.getAllBooks();
        for (int i = 0; i < BookList.size(); i++) {
        	Book book = BookList.get(i);
        	Category category= categoryService.getCategory(book.getCategory_id());
        	book.setBookCategory(category);
        }
        return new ModelAndView("bookList", "bookList", BookList);
    }
    
    @RequestMapping("searchBook")
    public ModelAndView searchBook(@RequestParam("searchName") String searchName) {  
        logger.info("Searching the Book. Book Names: "+searchName);
        List<Book> BookList = BookService.getAllBooks(searchName);
        return new ModelAndView("bookList", "bookList", BookList);      
    }
    @RequestMapping("books")
    public ModelAndView searchBook(@RequestParam("cateid") int cateid) {  
        logger.info("Searching the Book. Book Names: "+cateid);
        List<Book> BookList = BookService.getAllBooks(cateid);
        return new ModelAndView("books", "bookList", BookList);      
    }
    
    @RequestMapping("bookDetail")
    public ModelAndView getBook(@RequestParam("id") int id) {  
        logger.info("Searching the Book: "+id);
        Book book = BookService.getBook(id);
        return new ModelAndView("bookDetail", "book", book);      
    }
}
