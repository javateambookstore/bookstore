package com.vin.demo.service;

import java.util.List;

import com.vin.demo.entity.OrderItem;

public interface OrderItemService {
	public int createOrderItem(OrderItem orderItem);
    public OrderItem updateOrderItem(OrderItem orderItem);
    public void deleteOrderItem(int id);
    public List<OrderItem> getAllOrderItems();
    public OrderItem getOrderItem(int id);   
    public List<OrderItem> getOrderItemsByOrderId(int orderId);
}
