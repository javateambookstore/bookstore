package com.vin.demo.service;

import java.math.BigDecimal;
import java.util.List;

import com.vin.demo.entity.Orders;

public interface OrdersService {
	Orders getOrdersById(int orderId);
	void update(Orders order);
	long insertOrder(Orders order);
	BigDecimal getOrderGrandTotal(int orderId);
	List<Orders> getAllOrders();
}
