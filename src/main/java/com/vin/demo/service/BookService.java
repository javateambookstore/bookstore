package com.vin.demo.service;

import java.util.List;

import com.vin.demo.entity.Book;

public interface BookService {
	  public int createBook(Book Book);
	    public Book updateBook(Book Book);
	    public void deleteBook(int id);
	    public List<Book> getAllBooks();
	    public Book getBook(int id);   
	    public List<Book> getAllBooks(String BookName);
	    public List<Book> getAllBooks(int cateid);
}
