package com.vin.demo.service;

import java.util.List;

import com.vin.demo.entity.Category;

public interface CategoryService {
	    public int createCategory(Category category);
	    public Category updateCategory(Category category);
	    public void deleteCategory(int id);
	    public List<Category> getAllCategorys();
	    public Category getCategory(int id);   
	    public List<Category> getAllCategorys(String categoryName);
}
