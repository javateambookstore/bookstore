package com.vin.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vin.demo.dao.CategoryDAO;
import com.vin.demo.entity.Category;
import com.vin.demo.service.CategoryService;
@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {
	@Autowired
	private CategoryDAO CategoryDAO;

	@Override
	public int createCategory(Category category) {
		// TODO Auto-generated method stub
		return CategoryDAO.createCategory(category);
	}

	@Override
	public Category updateCategory(Category category) {
		return CategoryDAO.updateCategory(category);
	}

	@Override
	public void deleteCategory(int id) {
		CategoryDAO.deleteCategory(id);
	}

	@Override
	public List<Category> getAllCategorys() {
		return CategoryDAO.getAllCategorys();
	}

	@Override
	public Category getCategory(int id) {
		return CategoryDAO.getCategory(id);

	}

	@Override
	public List<Category> getAllCategorys(String categoryName) {
		// TODO Auto-generated method stub
		return CategoryDAO.getAllCategorys(categoryName);
	}
}
