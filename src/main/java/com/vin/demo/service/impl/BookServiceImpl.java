package com.vin.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vin.demo.dao.BookDAO;
import com.vin.demo.entity.Book;
import com.vin.demo.service.BookService;

@Service
@Transactional
public class BookServiceImpl implements BookService{
	@Autowired
	private BookDAO bookDAO;
	@Override
	public int createBook(Book book) {
		// TODO Auto-generated method stub
		return bookDAO.createBook(book);
	}

	@Override
	public Book updateBook(Book book) {
		return bookDAO.updateBook(book);
	}

	@Override
	public void deleteBook(int id) {
		 bookDAO.deleteBook(id);
		
	}

	@Override
	public List<Book> getAllBooks() {
		return bookDAO.getAllBooks();
	}

	@Override
	public Book getBook(int id) {
		// TODO Auto-generated method stub
		return bookDAO.getBook(id);
	}

	@Override
	public List<Book> getAllBooks(String bookName) {
		// TODO Auto-generated method stub
		return bookDAO.getAllBooks(bookName);
	}
	@Override
	public List<Book> getAllBooks(int cateid) {
		// TODO Auto-generated method stub
		return bookDAO.getAllBooks(cateid);
	}
}
