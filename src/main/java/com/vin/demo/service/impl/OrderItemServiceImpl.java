package com.vin.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vin.demo.dao.OrderItemDAO;
import com.vin.demo.entity.OrderItem;
import com.vin.demo.service.OrderItemService;
@Service
@Transactional
public class OrderItemServiceImpl implements OrderItemService {
	@Autowired
	private OrderItemDAO orderItemDAO;
	@Override
	public int createOrderItem(OrderItem orderItem) {
		// TODO Auto-generated method stub
		return orderItemDAO.createOrderItem(orderItem);
	}

	@Override
	public OrderItem updateOrderItem(OrderItem orderItem) {
		// TODO Auto-generated method stub
		return orderItemDAO.updateOrderItem(orderItem);
	}

	@Override
	public void deleteOrderItem(int id) {
		// TODO Auto-generated method stub
		orderItemDAO.deleteOrderItem(id);
	}

	@Override
	public List<OrderItem> getAllOrderItems() {
		// TODO Auto-generated method stub
		return orderItemDAO.getAllOrderItems();
	}

	@Override
	public OrderItem getOrderItem(int id) {
		// TODO Auto-generated method stub
		return orderItemDAO.getOrderItem(id);
	}

	@Override
	public List<OrderItem> getOrderItemsByOrderId(int orderId) {
		// TODO Auto-generated method stub
		return orderItemDAO.getOrderItemsByOrderId(orderId);
	}

}
