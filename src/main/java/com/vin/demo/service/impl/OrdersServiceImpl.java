package com.vin.demo.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vin.demo.dao.OrdersDAO;
import com.vin.demo.entity.OrderItem;
import com.vin.demo.entity.Orders;
import com.vin.demo.service.OrdersService;
@Service
@Transactional
public class OrdersServiceImpl  implements OrdersService{

	@Autowired
	private OrdersDAO orderDao;
	@Override
	public Orders getOrdersById(int orderId) {
		return orderDao.getOrderById(orderId);
	}

	@Override
	public void update(Orders order) {
		 orderDao.update(order);
		
	}

	@Override
	public BigDecimal getOrderGrandTotal(int orderId) {
		BigDecimal grandTotal = new BigDecimal(0);
	        Orders order = getOrdersById(orderId);
	        List<OrderItem> orderItems = order.getOrderItems();
	        for (OrderItem item : orderItems){
	            grandTotal = grandTotal.add(item.getTotalPrice());
	        }

	        return grandTotal;
	}

	@Override
	public long insertOrder(Orders order) {
		return orderDao.insertOrder(order);
	}
	@Override
	public List<Orders> getAllOrders() {
		// TODO Auto-generated method stub
		return orderDao.getAllOrders();
	}
}
