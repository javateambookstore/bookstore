package com.vin.demo.dao;

import java.util.List;

import com.vin.demo.entity.Category;

public interface CategoryDAO {
	    public int createCategory(Category category);
	    public Category updateCategory(Category category);
	    public void deleteCategory(int id);
	    public List<Category> getAllCategorys();
	    public Category getCategory(int id);   
	    public List<Category> getAllCategorys(String categoryName);
}
