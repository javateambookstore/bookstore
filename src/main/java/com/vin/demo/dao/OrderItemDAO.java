package com.vin.demo.dao;

import java.util.List;

import com.vin.demo.entity.OrderItem;

public interface OrderItemDAO {
	public int createOrderItem(OrderItem orderItem);
    public OrderItem updateOrderItem(OrderItem orderItem);
    public void deleteOrderItem(int id);
    public List<OrderItem> getAllOrderItems();
    public OrderItem getOrderItem(int id);   
    public List<OrderItem> getOrderItemsByOrderId(int orderId);
}
