package com.vin.demo.dao.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.vin.demo.dao.CategoryDAO;
import com.vin.demo.entity.Category;
import com.vin.demo.util.HibernateUtil;
@Repository
public class CategoryDAOImpl implements CategoryDAO {

	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public int createCategory(Category category) {
		return (int) hibernateUtil.create(category);
	}

	@Override
	public Category updateCategory(Category category) {
		return hibernateUtil.update(category);
	}

	@Override
	public void deleteCategory(int id) {
		// TODO Auto-generated method stub
		Category category = new Category();
		category.setCategoryId(id);
		hibernateUtil.delete(category);

	}

	@Override
	public List<Category> getAllCategorys() {
		return hibernateUtil.fetchAll(Category.class);
	}

	@Override
	public Category getCategory(int id) {
		return hibernateUtil.fetchById(id, Category.class);
	}

	@Override
	public List<Category> getAllCategorys(String categoryName) {
		String query = "SELECT e.* FROM category e WHERE e.name like '%" + categoryName + "%'";
		List<Object[]> categoryObjects = hibernateUtil.fetchAll(query);
		List<Category> categories = new ArrayList<Category>();
		for (Object[] CategoryObject : categoryObjects) {
			Category Category = new Category();
			int id = ((Integer) CategoryObject[0]).intValue();
			String name = (String) CategoryObject[1];
			String description = (String) CategoryObject[3];
			Category.setCategoryId(id);
			Category.setCategoryName(name);
			Category.setCategoryDescription(description);
			categories.add(Category);
		}
		System.out.println(categories);
		return categories;
	}

}
