package com.vin.demo.dao.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.vin.demo.dao.OrdersDAO;
import com.vin.demo.entity.Category;
import com.vin.demo.entity.Orders;
import com.vin.demo.util.HibernateUtil;

@Repository
public class OrdersDAOImpl implements OrdersDAO {
	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public Orders getOrderById(int OrderId) {
		return hibernateUtil.fetchById(OrderId, Orders.class);
	}

	@Override
	public Orders validate(int OrderId) throws IOException {
		Orders orders = getOrderById(OrderId);
		// if(orders == null || orders.getOrdersItems().size() == 0){
		// throw new IOException(OrderId + "");
		// }
		update(orders);
		return orders;
	}

	@Override
	public void update(Orders Order) {
		hibernateUtil.update(Order);
	}

	@Override
	public long insertOrder(Orders order) {
		// TODO Auto-generated method stub
		return (long) hibernateUtil.create(order);
	}
	@Override
	public List<Orders> getAllOrders() {
		return hibernateUtil.fetchAll(Orders.class);
	}
}
