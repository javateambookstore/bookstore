package com.vin.demo.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.vin.demo.dao.BookDAO;
import com.vin.demo.dao.OrderItemDAO;
import com.vin.demo.entity.Book;
import com.vin.demo.entity.OrderItem;
import com.vin.demo.service.BookService;
import com.vin.demo.util.HibernateUtil;
@Repository
public class OrderItemDAOImpl implements OrderItemDAO{
	@Autowired
	private HibernateUtil hibernateUtil;
	
	@Autowired
	private BookDAO bookDAO;
	
	@Override
	public int createOrderItem(OrderItem orderItem) {
		// TODO Auto-generated method stub
		return (Integer) hibernateUtil.create(orderItem);
	}

	@Override
	public OrderItem updateOrderItem(OrderItem orderItem) {
		// TODO Auto-generated method stub
		return hibernateUtil.update(orderItem);
	}

	@Override
	public void deleteOrderItem(int id) {
		OrderItem orderItem = new OrderItem();
		orderItem.setId(id);
		hibernateUtil.delete(orderItem);
	}

	@Override
	public List<OrderItem> getAllOrderItems() {
		return hibernateUtil.fetchAll(OrderItem.class);
	}

	@Override
	public OrderItem getOrderItem(int id) {
		return hibernateUtil.fetchById(id, OrderItem.class);
	}

	@Override
	public List<OrderItem> getOrderItemsByOrderId(int orderId) {
		String query = "SELECT e.* FROM OrderItem e WHERE e.orderId=" + orderId;
		List<Object[]> OrderItemObjects = hibernateUtil.fetchAll(query);
		List<OrderItem> OrderItems = new ArrayList<OrderItem>();
		for (Object[] OrderItemObject : OrderItemObjects) {
			OrderItem orderItem = new OrderItem();
			int id = ((Integer) OrderItemObject[0]).intValue();
			BigDecimal price = (BigDecimal) OrderItemObject[1];			
			int quantity = ((Integer) OrderItemObject[2]).intValue();
			BigDecimal totalPrice = (BigDecimal) OrderItemObject[3];
			int bookId = (int)OrderItemObject[4];
			Book book = bookDAO.getBook(bookId);
			orderItem.setId(id);
			orderItem.setPrice(price);
			orderItem.setQuantity(quantity);
			orderItem.setTotalPrice(totalPrice);
			orderItem.setBook(book);
			
			OrderItems.add(orderItem);
		}
		System.out.println(OrderItems);
		return OrderItems;
	}

}
