package com.vin.demo.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.vin.demo.dao.BookDAO;
import com.vin.demo.entity.Book;
import com.vin.demo.util.HibernateUtil;

@Repository

public class BookDAOImpl implements BookDAO {

	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public int createBook(Book Book) {
		return (Integer) hibernateUtil.create(Book);
	}

	@Override
	public Book updateBook(Book Book) {
		return hibernateUtil.update(Book);
	}

	@Override
	public void deleteBook(int id) {
		// TODO Auto-generated method stub
		Book Book = new Book();
		Book.setId(id);
		hibernateUtil.delete(Book);

	}

	@Override
	public List<Book> getAllBooks() {
		return hibernateUtil.fetchAll(Book.class);
	}

	@Override
	public Book getBook(int id) {
		return hibernateUtil.fetchById(id, Book.class);
	}

	@Override
	public List<Book> getAllBooks(String BookName) {
		String query = "SELECT e.* FROM Book e WHERE e.name like '%" + BookName + "%'";
		List<Object[]> BookObjects = hibernateUtil.fetchAll(query);
		List<Book> Books = new ArrayList<Book>();
		for (Object[] BookObject : BookObjects) {
			Book book = new Book();
		
			int id = ((Integer) BookObject[0]).intValue();
			String authors = (String) BookObject[1];
			String code = (String) BookObject[2];
			String isbn = (String) BookObject[3];
			String name = (String) BookObject[4];
			BigDecimal price = (BigDecimal) BookObject[5];
			Date published_date = (Date) BookObject[6];
			String publisher = (String) BookObject[7];
			int categoryId = ((Integer) BookObject[9]).intValue();
			String description = (String) BookObject[10];
			String image = (String) BookObject[11];
			
			book.setId(id);
			book.setAuthors(authors);
			book.setCode(code);
			book.setIsbn(isbn);
			book.setName(name);
			book.setPrice(price);
			book.setPublished_date(published_date);
			book.setPublisher(publisher);
			book.setCategory_id(categoryId);
			book.setDescription(description);
			book.setImage(image);
			Books.add(book);
		}
		System.out.println(Books);
		return Books;
	}

	@Override
	public List<Book> getAllBooks(int cateid) {
		String query = "SELECT e.* FROM Book e WHERE e.category_id=" + cateid;
		List<Object[]> BookObjects = hibernateUtil.fetchAll(query);
		List<Book> Books = new ArrayList<Book>();
		for (Object[] BookObject : BookObjects) {
			Book book = new Book();
			int id = ((Integer) BookObject[0]).intValue();
			String authors = (String) BookObject[1];
			String code = (String) BookObject[2];
			String isbn = (String) BookObject[3];
			String name = (String) BookObject[4];
			BigDecimal price = (BigDecimal) BookObject[5];
			Date published_date = (Date) BookObject[6];
			String publisher = (String) BookObject[7];
			int categoryId = ((Integer) BookObject[9]).intValue();
			String description = (String) BookObject[10];
			String image = (String) BookObject[11];
	
			book.setId(id);
			book.setAuthors(authors);
			book.setCode(code);
			book.setIsbn(isbn);
			book.setPublished_date(published_date);
			book.setName(name);
			book.setPrice(price);
			book.setPublisher(publisher);
			book.setCategory_id(categoryId);
			book.setDescription(description);
			book.setImage(image);
			Books.add(book);
		}
		System.out.println(Books);
		return Books;
	}
}
