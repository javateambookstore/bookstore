package com.vin.demo.dao;

import java.io.IOException;
import java.util.List;

import com.vin.demo.entity.Orders;

public interface OrdersDAO {
		Orders getOrderById(int OrderId);
	    Orders validate(int OrderId) throws IOException;
	    void update(Orders Order);
	    long insertOrder(Orders Order);
	    List<Orders> getAllOrders();
}
