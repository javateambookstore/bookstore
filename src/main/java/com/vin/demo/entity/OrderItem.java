package com.vin.demo.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "OrderItem")
public class OrderItem {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column
	private BigDecimal price;
	@Column
	private int quantity;
	@Column
	private BigDecimal totalPrice;
	
	@ManyToOne(targetEntity=Orders.class,fetch = FetchType.LAZY)
	@JoinColumn(name = "orderid")
	private Orders order;
	
	@ManyToOne(targetEntity=Book.class,fetch = FetchType.EAGER)
	@JoinColumn(name = "bookid")
	private Book book;
	

	public Book getBook() {
		return book;
	}

	
	public OrderItem() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Orders getOrder() {
		return order;
	}

	public void setOrder(Orders order) {
		this.order = order;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	public void setBook(Book book) {
		this.book = book;
	}
}
