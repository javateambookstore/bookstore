package com.vin.demo.entity;

import java.util.List;

public class BookModel {
	private Book book;
	private List<Category> categories;
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public List<Category> getCategories() {
		return categories;
	}
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
}
