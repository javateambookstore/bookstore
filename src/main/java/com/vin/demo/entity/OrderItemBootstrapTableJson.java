package com.vin.demo.entity;

import java.util.List;

public class OrderItemBootstrapTableJson {
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public List<OrderItem> getRows() {
		return rows;
	}
	public void setRows(List<OrderItem> rows) {
		this.rows = rows;
	}
	public int total;
	public List<OrderItem> rows;
}
