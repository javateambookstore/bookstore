<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Trang bán sách online</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet"> 
<style type="text/css">
.myrow-container {
	margin: 20px;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/pages/shared/headernoslide.jsp"></jsp:include>
	<div class="container">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title">
					<div align="left">
						<b>Giỏ hàng</b>
					</div>

				</h3>
			</div>
			<div class="panel-body">
				<c:if test="${empty order.orderItems}">
                There are no Book
            </c:if>
				<c:if test="${not empty order.orderItems}">

					<table class="table table-hover table-bordered">
						<thead style="background-color: #bce8f1;">
							<tr>

								<th>BookName</th>
								<th>Authors</th>
								<th>Price</th>
								<th>Subtraction</th>
								<th>Quantity</th>
								<th>Addition</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${order.orderItems}" var="emp">
								<tr>
									<th><c:out value="${emp.book.name}" /></th>
									<th><c:out value="${emp.book.authors}" /></th>
									<th><c:out value="${emp.book.price}" /></th>
									<th><a
										href="editToCart?bookId=<c:out value='${emp.book.id}'/>&value=-1">-</a></th>
									<th><c:out value="${emp.quantity}" /></th>
									<th><a
										href="editToCart?bookId=<c:out value='${emp.book.id}'/>&value=1">+</a></th>
									<th><a
										href="deleteToCart?bookId=<c:out value='${emp.book.id}'/>">Delete</a></th>
								</tr>
							</c:forEach>
							<tr>
								<th></th>
								<th></th>
								<th>Grand Total</th>
								<th><fmt:formatNumber type="number" maxFractionDigits="3"
										value="${order.grandTotal }" /></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tbody>
					</table>
					 <a href="checkOut" class="btn btn-success pull-right"><span class="glyphicon glyphicon-shopping-cart"></span> Check out</a>
					  <a href="Home" class="btn btn-default">Continue Shopping</a>
				</c:if>
			</div>
		</div>

	</div>

	<jsp:include page="/WEB-INF/pages/shared/footer.jsp"></jsp:include>

</body>
</html>