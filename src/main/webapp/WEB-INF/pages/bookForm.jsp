<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="nqvinhtong">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Book Information</title>
<!-- Bootstrap CSS -->
<%-- <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet"> --%>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<style type="text/css">
.myrow-container {
	margin: 20px;
}
</style>
</head>
<body class=".container-fluid">
	<div class="container">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title">Book Details</h3>
			</div>
			<div class="panel-body">
				<form:form id="BookRegisterForm" cssClass="form-horizontal"
					modelAttribute="book" method="post" action="saveBook">
					<div class="form-group">
						<form:label path="category_id" cssClass="control-label col-xs-3">Category</form:label>
						<div class="col-xs-6">
							<form:hidden path="category_id"
								value="${bookObject.book.category_id}" />
							<div class="dropdown">
								<select class="form-control" id="category_id_select"
									onchange="onchangeSelect()">
									<c:forEach items="${bookObject.categories}" var="emp">
										<option value="${emp.categoryId}">${emp.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>
					</div>

					<div class="form-group">
						<%-- <div class="control-label col-xs-3">
							<form:label path="name">Book Code</form:label>
						</div> --%>
						<div class="col-xs-6">
							<form:hidden path="code" value="${bookObject.book.code}" />
<%-- 
							<form:input cssClass="form-control" path="code"
								value="${bookObject.book.code}" /> --%>
						</div>
					</div>
					<div class="form-group">
						<div class="control-label col-xs-3">
							<form:label path="name">Book Name</form:label>
						</div>
						<div class="col-xs-6">
							<form:hidden path="id" value="${bookObject.book.id}" />

							<form:input cssClass="form-control" path="name"
								value="${bookObject.book.name}" />
						</div>
					</div>

					<div class="form-group">
						<div class="control-label col-xs-3">
							<form:label path="image">Book Image</form:label>
						</div>
						<div class="col-xs-6">
							<form:input cssClass="form-control" path="image"
								value="${bookObject.book.image}" />
						</div>
					</div>

					<div class="form-group">
						<div class="control-label col-xs-3">
							<form:label path="authors">Authors</form:label>
						</div>
						<div class="col-xs-6">
							<form:input cssClass="form-control" path="authors"
								value="${bookObject.book.authors}" />
						</div>
					</div>

					<div class="form-group">
						<div class="control-label col-xs-3">
							<form:label path="price">Price</form:label>
						</div>
						<div class="col-xs-6">
							<form:input cssClass="form-control" path="price"
								value="${bookObject.book.price}" />
						</div>
					</div>

					<div class="form-group">
						<div class="control-label col-xs-3">
							<form:label path="description">Book Description</form:label>
						</div>
						<div class="col-xs-6">
							<form:input cssClass="form-control" path="description"
								value="${bookObject.book.description}" />
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-xs-4"></div>
							<div class="col-xs-4">
								<input type="submit" id="saveBook" class="btn btn-primary"
									value="Save" onclick="return submitBookForm();" />
							</div>
							<div class="col-xs-4"></div>
						</div>
					</div>

				</form:form>
			</div>
		</div>
	</div>

	<script src="<c:url value="/resources/js/jquery-1.9.1.min.js" />"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

	<script type="text/javascript">
	    $(document).ready(function(){
			var value= $('#category_id').val();console.log(value);
			$("#category_id_select").val(value).change();;
			
		});
		function onchangeSelect() {
			var value = $('#category_id_select :selected').val();
			console.log(value);
			$('#category_id').val(value);

		};
		function submitBookForm() {
			var cateid = $('#category_id').val().trim();
			if (cateid == 0)
				$('#category_id').val($('#category_id_select :selected').val());
			// getting the employee form values
			var name = $('#name').val().trim();
			var price = $('#price').val();
			if (name.length == 0) {
				alert('Please enter name');
				$('#name').focus();
				return false;
			}

			if (price <= 0) {
				alert('Please enter price');
				$('#price').focus();
				return false;
			}
			return true;
		};
	</script>

</body>
</html>