<%@page import="org.springframework.context.annotation.Import"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.vin.demo.entity.OrderItem,com.vin.demo.entity.Orders" %>
<link href="<c:url value="/resources/css/main.css" />"
	rel="stylesheet">
<div class="container">
		<nav class="navbar navbar-inverse navbar-static-top" role="navigation"
			style="margin-bottom: 0px;">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">BookStore</span> <span class="icon-bar"></span>
					<span class="icon-bar"></span> <span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="Home">Home</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="books?cateid=3">Sách cuộc sống</a></li>
					<li><a href="books?cateid=4">Sách kinh tế</a></li>
					<li><a href="books?cateid=5">Sách văn học</a></li>
					<!-- <li><a href="getAllCategories">Category</a></li>
					<li><a href="getAllBooks">Book</a></li> -->
					<li class="cart"><a class="ca" href="cart" >Cart 
					<span class="counter" id="cartnumber">
					<% Orders order= (Orders) session.getAttribute("CARTOBJECT"); if(order==null) order= new Orders();  %>
					<% int number = 0;
						for(int i=0;i<order.getOrderItems().size();i++){
							OrderItem item = order.getOrderItems().get(i);
							number += item.getQuantity();
						}					
					%>
					<%= number %>
					</span>				
					</a></li>
				</ul>
				<ul>
				</ul>
				
				 
			</div>
		</div>
		</nav>

	</div>

