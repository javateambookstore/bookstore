<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Chi tiết</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<style type="text/css">
.myrow-container {
	margin: 20px;
}

.glyphicon {
	margin-right: 5px;
}

.thumbnail {
	margin-bottom: 20px;
	padding: 0px;
	-webkit-border-radius: 0px;
	-moz-border-radius: 0px;
	border-radius: 0px;
}

.item.list-group-item {
	float: none;
	width: 100%;
	background-color: #fff;
	margin-bottom: 10px;
}

.item.list-group-item:nth-of-type(odd):hover, .item.list-group-item:hover
	{
	background: #428bca;
}

.item.list-group-item .list-group-image {
	margin-right: 10px;
}

.item.list-group-item .thumbnail {
	margin-bottom: 0px;
}

.item.list-group-item .caption {
	padding: 9px 9px 0px 9px;
}

.item.list-group-item:nth-of-type(odd) {
	background: #eeeeee;
}

.item.list-group-item:before, .item.list-group-item:after {
	display: table;
	content: " ";
}

.item.list-group-item img {
	float: left;
}

.item.list-group-item:after {
	clear: both;
}

.list-group-item-text {
	margin: 0 0 11px;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/pages/shared/headernoslide.jsp"></jsp:include>
	<c:if test="${empty book}">
		<div class="container">Not exist this book</div>
	</c:if>
	<div class="container">
		<div id="product">
			<c:if test="${not empty book}">
				<div class="well well-sm">
					<strong>${book.name}</strong>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<a href="bookDetail?id=${book.id}" class="thumbnail"> <img
							class="group list-group-image" src="${book.image}" alt=""
							onerror="this.src='http://placehold.it/400x250/000/fff';" />
						</a>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="caption">
							<h4 class="group inner list-group-item-heading">${book.name}</h4>
							<p class="group inner list-group-item-text">${book.authors}</p>
							<div class="row">
								<div class="col-xs-12 col-md-12">
									<p class="lead">
										<fmt:formatNumber type="number" maxFractionDigits="3"
											value="${book.price}" />
										đ
									</p>
								</div>
								<div class="col-xs-12 col-md-12">
								
									<a class="btn btn-success"
											onclick="addToCart('addToCart?bookId=${book.id}')"
											href="javascript:void(0)">Add to cart</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-12">
						<br /> ${book.description} <br /> <br />
					</div>
				</div>
			</c:if>
		</div>
	</div>
	<div class="container"></div>
	<jsp:include page="/WEB-INF/pages/shared/footer.jsp"></jsp:include>
</body>
</html>