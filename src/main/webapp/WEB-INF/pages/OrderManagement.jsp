<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Danh sách đơn hàng</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link	src="<c:url value="/resources/css/bootstrap-table.css" />"></link>
<style type="text/css">
.myrow-container {
	margin: 20px;
}
</style>
<body class=".container-fluid">
	<jsp:include page="/WEB-INF/pages/shared/headerAdmin.jsp"></jsp:include>

	<div class="container">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title">
					<div align="left">
						<b>Danh sách đơn hàng</b>
					</div>
				</h3>
			</div>
			<div class="panel-body">
				<c:if test="${empty ordersList}">
                There are no order
            </c:if>
				<c:if test="${not empty ordersList}">
					<form action="searchBook">
						<div class="row">
							<div class="col-md-4">
								Search Orders: <input type='text' name='searchName'
									id='searchName' />
							</div>
							<div class="col-md-4">
								<input class="btn btn-success" type='submit' value='Search' />
							</div>
						</div>
					</form>
					<table class="table table-hover table-bordered">
						<thead style="background-color: #bce8f1;">
							<tr>
								<th>id</th>
								<th>customerName</th>
								<th>customerAddress</th>
								<th>shippingAddress</th>
								<th>email</th>
								<th>phoneNumber</th>
								<th>grandTotal</th>
								<th>Chi tiết</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${ordersList}" var="emp">
								<tr>
									<th><c:out value="${emp.id}" /></th>
									<th><c:out value="${emp.customerName}" /></th>
									<th><c:out value="${emp.customerAddress}" /></th>
									<th><c:out value="${emp.shippingAddress}" /></th> 
									<th><c:out value="${emp.email}" /></th>
									<th><c:out value="${emp.phoneNumber}" /></th>
									<th><c:out value="${emp.grandTotal}" /></th>
									<td><input id="${emp.id}" type="text" class="btn btn-success" value="chi tiết" onclick="detailOrders(${emp.id})"/></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
			</div>
		</div>
		<!-- Modal -->
		  <div class="modal fade" id="myModal" role="dialog">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Chi tiết đơn hàng</h4>
		        </div>
		        <div class="modal-body">
		          <table id="orderDetail"></table>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		      
		    </div>
		  </div>
		<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script
			src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script	src="<c:url value="/resources/js/bootstrap-table.js" />"></script>
		<script	src="<c:url value="/resources/js/bootstrap-table-filter.js" />"></script>
		<script>
			function detailOrders(orderId){
				$("#myModal").modal();
				$("#myModal").on('hidden.bs.modal', function (event) {
		            $('#orderDetail').bootstrapTable('destroy');
		        });
				$("#orderDetail").bootstrapTable({
	                idField: 'id',
	                method: 'get',
	                url: 'Orders/Item',
	                pagination: false,
	                showToggle: true,
	                minimumCountColumns: 1,
	                pageSize: 50,
	                pageList: [5, 10, 25, 50, 100, 200],
	                showColumns: true,
	                showRefresh: false,
	                clickToSelect: false,
	                sortName: 'id',
	                sidePagination: 'server',
	                queryParams: function (p) {
	                    return {
	                    	orderId: orderId,
	                        sort: p.sort,
	                        order: p.order,
	                        limit: p.limit,
	                        offset: p.offset
	                    };
	                },
	                columns: [
	                    {
	                        field: 'id',
	                        title: 'id',
	                        align: 'right',
	                        valign: 'middle',
	                        sortable: true
	                    },
	                    {
	                        field: 'book.name',
	                        title: 'Name',
	                        align: 'right',
	                        valign: 'middle',
	                        sortable: true
	                    },
	                    {
	                        field: 'price',
	                        title: 'Đơn giá (VNĐ)',
	                        align: 'right',
	                        valign: 'middle',
	                        sortable: true
	                    }, {
	                        field: 'quantity',
	                        title: 'Số lượng',
	                        align: 'right',
	                        valign: 'middle',
	                        sortable: true
	                    },
	                    {
	                        field: 'totalPrice',
	                        title: 'Tổng tiền (VNĐ)',
	                        align: 'right',
	                        valign: 'middle',
	                        sortable: true
	                    }
	                ]
	            });
			} 
		</script>
</body>
</html>