<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Danh sách sản phẩm</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet"> 
<style type="text/css">
.myrow-container {
	margin: 20px;
}

.glyphicon {
	margin-right: 5px;
}

.thumbnail {
	margin-bottom: 20px;
	padding: 0px;
	-webkit-border-radius: 0px;
	-moz-border-radius: 0px;
	border-radius: 0px;
}

.item.list-group-item {
	float: none;
	width: 100%;
	background-color: #fff;
	margin-bottom: 10px;
}

.item.list-group-item:nth-of-type(odd):hover, .item.list-group-item:hover
	{
	background: #428bca;
}

.item.list-group-item .list-group-image {
	margin-right: 10px;
}

.item.list-group-item .thumbnail {
	margin-bottom: 0px;
}

.item.list-group-item .caption {
	padding: 9px 9px 0px 9px;
}

.item.list-group-item:nth-of-type(odd) {
	background: #eeeeee;
}

.item.list-group-item:before, .item.list-group-item:after {
	display: table;
	content: " ";
}

.item.list-group-item img {
	float: left;
}

.item.list-group-item:after {
	clear: both;
}

.list-group-item-text {
	margin: 0 0 11px;
}
</style>
</head>
<body>
	<jsp:include page="/WEB-INF/pages/shared/header.jsp"></jsp:include>
	
	<div class="container">
		<div class="well well-sm">
			<strong>Cate 1</strong>
			<div class="btn-group">
				<a href="#" id="list" class="btn btn-default btn-sm"><span
					class="glyphicon glyphicon-th-list"> </span>List</a> <a href="#"
					id="grid" class="btn btn-default btn-sm"><span
					class="glyphicon glyphicon-th"></span>Grid</a>
			</div>
		</div>
		<div id="products" class="row list-group">
			<c:if test="${empty bookList}">
                <div>There are no book</div>
            </c:if>
            <c:if test="${not empty bookList}">
            	<c:forEach items="${bookList}" var="emp">
					<div class="item col-xs-4 col-lg-4">
						<div class="thumbnail">
						<a href="bookDetail?id=${emp.id}" > <img
								class="group list-group-image" src="${emp.image}" alt=""
								onerror="this.src='http://placehold.it/400x250/000/fff';" />
							</a>
							
							<div class="caption">
								<h4 class="group inner list-group-item-heading"><a href="bookDetail?id=${emp.id}">${emp.name}</a></h4>
								<p class="group inner list-group-item-text">${emp.authors}</p>
								<div class="row">
									<div class="col-xs-12 col-md-6">
										<p class="lead">$${emp.price}</p>
									</div>
									<div class="col-xs-12 col-md-6">
										<a class="btn btn-success" onclick="addToCart('addToCart?bookId=${emp.id}')"
											href="javascript:void(0)">Add to cart</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
            </c:if>
		</div>
	</div>
	
	<jsp:include page="/WEB-INF/pages/shared/footer.jsp"></jsp:include>
</body>
</html>