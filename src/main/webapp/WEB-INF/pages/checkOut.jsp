<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Tiến hành thanh toán</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet"> 
<style type="text/css">
.myrow-container {
	margin: 20px;
}
</style>
</head>
<body class=".container-fluid">
	<jsp:include page="/WEB-INF/pages/shared/headernoslide.jsp"></jsp:include>
	<div class="container">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title">Check out Details</h3>
			</div>
			<div class="panel-body">
				<form:form id="CheckOutRegisterForm" cssClass="form-horizontal"
					modelAttribute="order" method="post" action="saveCheckOut">


					<div class="form-group">
						<div class="control-label col-xs-3">
							<form:label path="customerName">Customer Name(*)</form:label>
						</div>
						<div class="col-xs-6">

							<form:input cssClass="form-control" path="customerName"
								value="${order.customerName}" />
						</div>
					</div>
					<div class="form-group">
						<div class="control-label col-xs-3">
							<form:label path="customerAddress">Customer Address</form:label>
						</div>
						<div class="col-xs-6">


							<form:input cssClass="form-control" path="customerAddress"
								value="${order.customerAddress}" />
						</div>
					</div>

					<div class="form-group">
						<div class="control-label col-xs-3">
							<form:label path="shippingAddress">Shipping Address(*)</form:label>
						</div>
						<div class="col-xs-6">
							<form:input cssClass="form-control" path="shippingAddress"
								value="${order.shippingAddress}" />
						</div>
					</div>

					<div class="form-group">
						<div class="control-label col-xs-3">
							<form:label path="email">Email(*)</form:label>
						</div>
						<div class="col-xs-6">
							<form:input cssClass="form-control" path="email"
								value="${order.email}" />
						</div>
					</div>

					<div class="form-group">
						<div class="control-label col-xs-3">
							<form:label path="phoneNumber">Phone Number(*)</form:label>
						</div>
						<div class="col-xs-6">
							<form:input cssClass="form-control" path="phoneNumber"
								value="${order.phoneNumber}" />
						</div>
					</div>


					<div class="form-group">
						<div class="row">
							<div class="col-xs-3"></div>
							<div class="col-xs-5">
								<input type="submit" id="checkOut" class="btn btn-primary pull-right"
									value="Save" onclick="return submitCheckOut();" />
							</div>
							<div class="col-xs-4"></div>
						</div>
					</div>

				</form:form>


				<table class="table table-hover table-bordered">
					<thead style="background-color: #bce8f1;">
						<tr>

							<th>BookName</th>
							<th>Authors</th>
							<th>Price</th>
							<th>Quantity</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${order.orderItems}" var="emp">
							<tr>
								<th><c:out value="${emp.book.name}" /></th>
								<th><c:out value="${emp.book.authors}" /></th>
								<th><c:out value="${emp.book.price}" /></th>
						
								<th><c:out value="${emp.quantity}" /></th>
								
							</tr>
						</c:forEach>
						<tr>
							<th></th>
							<th></th>
							<th>Grand Total</th>
							<th><fmt:formatNumber type="number" maxFractionDigits="3"
									value="${order.grandTotal }" /></th>
							
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<script src="<c:url value="/resources/js/jquery-1.9.1.min.js" />"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

	<script type="text/javascript">
		function submitCheckOut() {

			// getting the employee form values
			var name = $('#customerName').val().trim();
			if (name.length == 0) {
				alert('Please enter name');
				$('#customerName').focus();
				return false;
			}
			var shippingAddress = $('#shippingAddress').val().trim();
			if (name.length == 0) {
				alert('Please enter Shipping Address');
				$('#shippingAddress').focus();
				return false;
			}

			var email = $('#email').val().trim();
			if (name.length == 0) {
				alert('Please enter email');
				$('#email').focus();
				return false;
			}

			var email = $('#phoneNumber').val().trim();
			if (name.length == 0) {
				alert('Please enter Phone Number');
				$('#phoneNumber').focus();
				return false;
			}

			return true;
		};
	</script>

</body>
</html>